from unittest import TestCase,main
from client.service.carte_service import CarteService
from metier.carte import Carte


class TestCarteService(TestCase):
    def test_nb_carte_couleur(self):

        #On crée 2 cartes manuellement pour pouvoir tester la fonction de comptage
        json = [{
            "image": "https://deckofcardsapi.com/static/img/6C.png",
            "value": "6",
            "suit": "CLUBS",
            "code": "6C"
        }, {
            "image": "https://deckofcardsapi.com/static/img/4D.png",
            "value": "4",
            "suit": "DIAMONDS",
            "code": "4D"
        }]

        #On crée la liste de Carte à envoyer dans la fonction
        cartes = []
        for carte in json:
            cartes.append(Carte(**carte))

        #On teste la fonction avec ce qu'elle est censée renvoyer.
        self.assertEqual({
            "C": 1,
            "S": 0,
            "H": 0,
            "D": 1
        }, CarteService.nb_carte_couleur(cartes))




if __name__ == '__main__':
    main()
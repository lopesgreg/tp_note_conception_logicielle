Ce code tourne avec la version 3.8.1 de Python.
Pour le lancer, suivez les étapes !

-----------------------------------------------------

Etape 1 : Après avoir clone ce dépot, ouvrez un environnemenet virtuel sur VSCode par exemple à l'aide de la commande suivante : 

`python3 -m venv environnement-virtuel` 

Etape 2 : lancez l'environnement virtuel. La commande dépend du système d'exploitation que vous avez. Sur Mac, personnellement, j'utilise cette commande : 

`source environnement-virtuel/bin/activate` 

Pour un autre appareil que Mac, normalement, il faut lancer ceci : 

`source environnement-virtuel/Scripts/activate` 

Etape 3 : Récupérez les dépendances dont vous avez besoin en lançant le fichier requirements.txt :

`pip3 install -r requirements.txt` 

Etape 4 : Lancez le webservice pour démarrer l'api :

`python3 start_webservice.py`

Etape 5 : Ouvrez un autre terminal et lancez le client : 

`python3 start_client.py`


Note : si vous ne voulez pas créer d'environnement virtuel, vous pouvez vous mettre dans le dossier dans n'importe quel terminal et faire les étapes à partir de l'étape 3 (installation des dépendance).

----------------------------------------------------------------------

Pour lancer le fichier test :

`python -m unittest test.py`
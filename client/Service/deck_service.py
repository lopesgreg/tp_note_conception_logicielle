from metier.carte import Carte
from metier.deck_identifiant import DeckIdentifiant
import requests
from dotenv import dotenv_values


class DeckService:
    @staticmethod
    def creerDeck():

        config = dotenv_values(".env")
        api_host = config["API_HOST"]
        url = api_host + "/creer-un-deck/"
        
        reponse = requests.get(url)
        return reponse.json()
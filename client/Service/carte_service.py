from metier.carte import Carte
from metier.deck_identifiant import DeckIdentifiant
import requests
from dotenv import dotenv_values
from typing import List


class CarteService:
    @staticmethod
    def tirerCartesDansDeck(deck_id: str, nb_carte: int) :

        config = dotenv_values(".env")
        api_host = config["API_HOST"]
        url = api_host + "/cartes/" + str(nb_carte)

        reponse = requests.post(url,json = DeckIdentifiant(deck_id = deck_id).__dict__)
        cartes = []
        for carte in reponse.json():
            cartes.append(Carte(**carte))
        return cartes

    @staticmethod
    def nb_carte_couleur(liste_cartes: List[Carte]) :

        resultat = {"C": 0, "S": 0, "H": 0, "D": 0}
        for carte in liste_cartes:
            if carte.suit == "CLUBS":
                resultat["C"] += 1
            elif carte.suit == "SPADES":
                resultat["S"] += 1
            elif carte.suit == "HEARTS":
                resultat["H"] += 1
            elif carte.suit == "DIAMONDS":
                resultat["D"] += 1
        return resultat
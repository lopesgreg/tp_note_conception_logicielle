from PyInquirer import Separator, prompt
from client.menu.abstract_menu import AbstractMenu


class Menu_Carte(AbstractMenu):
    def __init__(self):
        self.question_deck = [{
            'type': 'input',
            'name': "choix_deck",
            'message': "Veuillez saisir le numéro du deck.\n"
        }]
        self.question_nb_carte = [{
            'type': 'input',
            'name': "choix_nb_carte",
            'message': "Veuillez saisir le nombre de cartes à tirer.\n"
        }]

    def display_info(self):
        pass

    def make_choice(self):
        id_deck = prompt(self.question_deck)
        continuer = True
        while continuer :
            try :
                choix = prompt(self.question_nb_carte)
                nb_carte = int(choix["choix_nb_carte"])
                continuer = False
            except: 
                print("Merci de rentrer un nombre entier.")
        from client.menu.menu_carte_detail import Menu_Carte_Detail
        return Menu_Carte_Detail(nb_carte = nb_carte, deck_id = id_deck["choix_deck"])

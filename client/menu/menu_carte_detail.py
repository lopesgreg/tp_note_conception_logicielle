from PyInquirer import Separator, prompt
from client.menu.abstract_menu import AbstractMenu
from client.service.carte_service import CarteService



class Menu_Carte_Detail(AbstractMenu):
    def __init__(self, nb_carte: int, deck_id: str):
        self.questions = [{
            'type': 'list',
            'name': "choix",
            'message': "Voici vos cartes",
            'choices': [Separator(), "Retour au menu"]
        }]
        self.nb_carte = nb_carte
        self.deck_id = deck_id

    def display_info(self):
        try:
            cartes = CarteService.tirerCartesDansDeck(deck_id = self.deck_id, nb_carte = self.nb_carte)

            print("Votre numéro de deck est le " + self.deck_id + "\n")

            main = []
            for carte in cartes:
                main.append(carte.value + " " + carte.suit)
            print("Vous avez tiré " + str(len(cartes)) + " carte(s) de ce deck.")


            print("Les cartes que vous avez tirées sont : " + str(main)+"\n")
            print("Voici le détail du nombre de carte par couleur :  ")
            print(CarteService.nb_carte_couleur(cartes) + "\n")

        except:
            print("Impossible de récuperer une main depuis ce deck veuillez rééssayer")

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse["choix"] == "Retour au menu":
            from client.menu.menu_depart import Menu_Depart
            return Menu_Depart()
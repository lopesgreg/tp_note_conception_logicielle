from PyInquirer import Separator, prompt
from client.menu.abstract_menu import AbstractMenu
from client.menu.menu_deck import Menu_Deck
from client.menu.menu_carte import Menu_Carte


class Menu_Depart(AbstractMenu):
    def __init__(self):

        self.questions = [{
            'type':
            'list',
            'name':
            "home",
            'message':"Choisissez l'action que vous voulez effectuer :",
            'choices': [
                "Récupérer un deck",
                Separator(),
                "Tirer des cartes dans un deck",
                Separator(),
                "Quitter l'application",
            ]
        }]

    def display_info(self):
        print("Bienvenu !!!!!")

    def make_choice(self):
        reponse = prompt(self.questions)

        if reponse["home"] == "Récupérer un deck":
            return Menu_Deck()

        elif reponse["home"] == "Tirer des cartes dans un deck":
            return Menu_Carte()
            
        else:
            pass
from PyInquirer import Separator, prompt
from client.menu.abstract_menu import AbstractMenu
from client.service.deck_service import DeckService





class Menu_Deck(AbstractMenu):
    def __init__(self):
        self.questions = [{
            'type': 'list',
            'name': "choix",
            'message': "",
            'choices': [Separator(), "Retour au menu"]
        }]

    def display_info(self):
        try:
            print("Votre numéro de deck est le " + DeckService.creerDeck())
        except:
            print("impossible de récuperer un deck, réessayez plus tard")

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse["choix"] == "Retour au menu":
            from client.menu.menu_depart import Menu_Depart
            return Menu_Depart()
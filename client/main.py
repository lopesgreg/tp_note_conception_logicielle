from client.menu.menu_depart import Menu_Depart


class App():
    def run():
        current_vue = Menu_Depart()
        while current_vue:
            current_vue.display_info()
            current_vue = current_vue.make_choice()

from pydantic import BaseModel

class Deck(BaseModel):
    success: bool
    deck_id: str
    shuffled: bool
    remaining: int
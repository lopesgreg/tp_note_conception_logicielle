from pydantic import BaseModel

class Carte(BaseModel):
    image: str
    value: str
    suit: str
    code: str
from pydantic import BaseModel
from metier.carte import Carte
from typing import List


class Cartes(BaseModel):
    success: bool
    cards: List[Carte]
    deck_id: str
    remaining: int
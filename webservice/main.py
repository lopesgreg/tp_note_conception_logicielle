from fastapi import FastAPI
from webservice.routers import deck_router
from webservice.routers import carte_router

app = FastAPI()

app.include_router(deck_router.router)
app.include_router(carte_router.router)
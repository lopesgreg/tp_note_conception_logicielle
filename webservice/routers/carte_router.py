from fastapi import APIRouter
from webservice.service.carte_service import CarteService
from metier.deck_identifiant import DeckIdentifiant

router = APIRouter()


@router.post("/cartes/{nombre_cartes}")
async def tirer_carte(nombre_cartes: int, deck: DeckIdentifiant):
    cartes = CarteService.tirerCartesDansDeck(deck.deck_id, nombre_cartes)
    return cartes.cards
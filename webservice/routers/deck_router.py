from fastapi import APIRouter
from webservice.service.deck_service import DeckService


router = APIRouter()


@router.get("/creer-un-deck/")
def read_root():
    deck = DeckService.creerDeck()
    return deck.deck_id
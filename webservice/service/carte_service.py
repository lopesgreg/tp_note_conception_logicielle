from metier.cartes import Cartes
from metier.cartes import Cartes
import requests


class CarteService:
    @staticmethod
    def tirerCartesDansDeck(deck_id: str, nb: int) :
        url = "https://deckofcardsapi.com/api/deck/" + deck_id + "/draw/?count=" + str(nb)
        reponse = requests.get(url)
        cartes = Cartes(**reponse.json())
        return cartes
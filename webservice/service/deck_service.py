from metier.deck import Deck
import requests


class DeckService:
    @staticmethod
    def creerDeck() :
        url = "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1"
        reponse = requests.get(url)
        deck = Deck(**reponse.json())
        return deck